from flask import Blueprint, request, jsonify
from services.user_service import UserService

user_api = Blueprint('user_api', __name__)

@user_api.route('/', methods=['get'])
def helloworld():
    return "Hello World PROYECTO-USERS!"

@user_api.route('/api/user', methods=['POST'])
def create_user_controller():

    data = request.get_json()
    data_id = UserService.create_user_service(data)
    return jsonify(message="User has been successfully created.", id=data_id), 201
